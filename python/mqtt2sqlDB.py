from asyncore import loop
from dataclasses import replace
from xmlrpc.client import DateTime
import paho.mqtt.client as mqtt
from secrets1 import pw, mqqtserver,mqttUser,mqttPassword,_url,_token,_org,_bucket
import os, time, sqlite3
import datetime
import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS
import ssl
dbPfad="/home/hal/scripte/ndw.db"
f=open("/home/hal/scripte/blacklist.txt","r")
#f=open("blacklist.txt","r")

blacklist=[]
for mac in f.readlines():
    blacklist.append(mac.strip())
print(blacklist)
f.close()

print(os.path.exists(dbPfad))
if not os.path.exists(dbPfad):
    connection1 = sqlite3.connect(dbPfad)
    cursor1 = connection1.cursor()


  
    sql = "CREATE TABLE NDW(time1 INTEGER,sniffer TEXT,mac TEXT,rssi INTEGER,ssid TEXT,seq INTEGER)"
    cursor1.execute(sql)
    sql = "CREATE TABLE eTwin(time1 INTEGER,sniffer TEXT,ssid TEXT, mac TEXT)"
    cursor1.execute(sql)
    connection1.close()



global db
global cursor
zähler50=[0,0,0,0,0]
zähler70=[0,0,0,0,0]
zählerGes=[0,0,0,0,0]

#bahn,Tcom,City,ICE,Vodafon
snName=['Sniffer_C55AAE','Sniffer_222E97','Sniffer_62EFB','Sniffer_368844','Sniffer_69D51C']
snLocation=['Eingang','SNES-CNTRL','Hauptschalter','VOID','3D-Drucker']


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("Sniffer/#")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    try:
        db = sqlite3.connect(dbPfad)
        cursor = db.cursor()
        print(msg.topic+" "+str(msg.payload))
        ms=msg.payload.decode().split(",")
        sniffer=ms[0]
        
        for i in range (len(snName)):
            if ms[0]==snName[i]:
                ms[0]=snLocation[i]
                break
        ti=str(time.time()).split(".")
        tim=ti[0]
        snif=""
        n=datetime.datetime.now()
        if len(ms)>7:
            if  not ms[3] in blacklist:
                print(ms)
                for i in range (len(snName)):
                    if sniffer==snName[i]:
                            
                        zählerGes[i] +=1
                
                if int(ms[1])>-70:
                    
                    for i in range (len(snName)):
                        if sniffer==snName[i]:
                            
                            zähler70[i] +=1
                elif int(ms[1])>-50:
                    
                    for i in range (len(snName)):
                        if sniffer==snName[i]:
                            
                            zähler50[i] +=1


                sql = "INSERT INTO NDW VALUES("
                sql += tim+","
                sql += "'"+ms[0]+"',"
                sql += "'"+ms[3]+"',"
                sql += ms[1]+","
                sql += "'"+ms[6]+"',"
                sql += ms[4]
                sql += ")"
                try:
                    cursor.execute(sql)
                except Exception as ex:
                    print(ex,sql)
                db.commit()
                localMac=0
                try:

                    localMac=(int(ms[3][0:2],16)&2)>>1
                except:
                    localMac=-1
                print(localMac)
                p = influxdb_client.Point("Sniffer").tag("Sniffer", ms[0]).tag("mac",ms[3]).field("rssi", ms[1]).time(n)
                write_api.write(bucket=_bucket, org=_org, record=p)

                p = influxdb_client.Point("Sniffer").tag("Sniffer", ms[0]).tag("mac",ms[3]).field("ssid",ms[6]).time(n)
                write_api.write(bucket=_bucket, org=_org, record=p)

                p = influxdb_client.Point("Sniffer").tag("Sniffer", ms[0]).tag("mac",ms[3]).field("SeqNr", ms[4]).time(n)
                write_api.write(bucket=_bucket, org=_org, record=p)


                #0= OUI enforced 1=local administered -1= not to read
                p = influxdb_client.Point("Sniffer").tag("Sniffer", ms[0]).tag("mac",ms[3]).field("randomMAC", str(localMac)).time(n)
                write_api.write(bucket=_bucket, org=_org, record=p)

                
            
                #p = influxdb_client.Point("eTwin").tag("Sniffer", ms[0]).tag("SSID",test).field("mac", ms[3]).time(n)    
        if len(ms)==3:
            p = influxdb_client.Point("eTwin").tag("Sniffer", ms[0]).tag("ssid",ms[1]).field("mac", ms[2]).time(n)
            write_api.write(bucket=_bucket, org=_org, record=p)
            #time1 INTEGER,sniffer TEXT,ssid TEXT, mac TEXT
            sql = "INSERT INTO eTwin VALUES("
            sql += tim+","
            sql += "'"+ms[0]+"',"
            sql += "'"+ms[1]+"',"
            sql += "'"+ms[2]+"'"
            
            sql += ")"

            cursor.execute(sql)
            db.commit()
            db.close()
        
    except Exception as e:
        print(e)
    
    
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
client.tls_set_context(context)
client.username_pw_set(mqttUser,mqttPassword)
client.tls_insecure_set(True)
client.connect("localhost", 8883, 60)




# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.



influxClient = influxdb_client.InfluxDBClient(
   url=_url,
   token=_token,
   org=_org,
   bucket=_bucket
)

write_api = influxClient.write_api(write_options=SYNCHRONOUS)

client.loop_start()


influxClient1 = influxdb_client.InfluxDBClient(
   url=_url,
   token=_token,
   org=_org,
   bucket=_bucket
)

write_api1 = influxClient1.write_api(write_options=SYNCHRONOUS)

while (True):
    
    time.sleep(60)
   
    n=datetime.datetime.now()
    for i in range(len(snName)):
        p = influxdb_client.Point("aktivität").tag("Sniffer", snLocation[i]).tag("typ","gesamt").field("connection", zählerGes[i]).time(n)
        write_api1.write(bucket=_bucket, org=_org, record=p)
        p = influxdb_client.Point("aktivität").tag("Sniffer", snLocation[i]).tag("typ","70").field("connection", zähler70[i]).time(n)
        write_api1.write(bucket=_bucket, org=_org, record=p)
        p = influxdb_client.Point("aktivität").tag("Sniffer", snLocation[i]).tag("typ","50").field("connection", zähler50[i]).time(n)
        write_api1.write(bucket=_bucket, org=_org, record=p)
        zähler70[i]=0
        zähler50[i]=0
        zählerGes[i]=0


