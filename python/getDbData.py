

from doctest import DONT_ACCEPT_TRUE_FOR_1
from datetime import datetime
from locale import D_T_FMT
from os import lseek
import shelve
import pygsheets
import pandas as pd
from OuiLookup import OuiLookup
from difflib import diff_bytes
import sqlite3
import time
#authorization
#fileName='/home/dotu/Documents/Projekte/spendendose/python/creds.json'
fileName='/home/hal/scripte/creds.json'
blackPfad="/home/hal/scripte/black"
dbPfad="/home/hal/scripte/ndw.db"
gc = pygsheets.authorize(service_file=fileName)
df = pd.DataFrame()




# Verbindung, Cursor



def getLocalMac(mac):
    localMac=0
    try:

        localMac=(int(mac[0:2],16)&2)>>1
    except:
        localMac=-1
    return localMac

#OuiLookup().update()
def getMacString(dat):
    v=str(dat)
    try:
        ret=v[0:2]+":"+v[2:4]+":"+v[4:6]+":"+v[6:8]+":"+v[8:10]+":"+v[10:12]
    except:
        ret="00:00:00:00"
    return ret

def writeSheet(sheet,df1):
 
    sh = gc.open('ndw22')
    wks = sh[sheet]
    wks.set_dataframe(df1,(1,1)) 

def makeDict(data,collumNr):
    di={}
    count=0
    
    for dat in data:
        count+=1
        if collumNr==-1:
            key=dat
        else:
            key=dat[collumNr]
            
        
        if key in di:
            di[key] +=1
        else:
            di[key] =1
    
    return di,count   
        

while True:
    # SQL-Abfrage
    connection = sqlite3.connect(dbPfad)
    cursor = connection.cursor()
    sql = "SELECT * FROM NDW "
    cursor.execute(sql)
    oldtime=0
    NdwCount=0
    uniqMAC=0
    #NDW(time1 INTEGER,sniffer TEXT,mac TEXT,rssi INTEGER,ssid TEXT,seq INTEGER)
    dfName=['zeit','sniffer','mac','rssi','ssid','seq']
    data0=[]
    data1=[]
    data2=[]
    data3=[]
    data4=[]
    data5=[]
    index=[]
    locMAC=0
    print("New DATA",datetime.now())
    countLocMac=0
    countSSID=0
    ssid=[]
    sql = "SELECT * FROM NDW "
    cursor.execute(sql)
    data,cnt =makeDict(cursor,2)
    cursor.execute(sql)
    datssid,cnt =makeDict(cursor,4)
    sql = "SELECT * FROM NDW "
    cursor.execute(sql)
    f = open(blackPfad,'w')
    for dsatz in cursor:
        NdwCount+=1
    
        val=list(dsatz)
        
            

        if val[4]!="":
            countSSID+=1
            ssid.append(val[4])

        data0.append(val[0])
        data1.append(val[1])
        data2.append(val[2])
        data3.append(val[3])
        data4.append(val[4])
        data5.append(val[5])
        index.append(int(val[2][0:2],16)&2)


    #df[dfName[0]]=data0
    #df[dfName[1]]=data1
    #df[dfName[2]]=data2
    #df[dfName[3]]=data3
    #df[dfName[4]]=data4
    #df[dfName[5]]=data5
    #df['index']=index


    #print(data)
    countLocMac=0
    for t in sorted(data.items(), key=lambda kv: kv[1]):
        locMAC=getLocalMac(t[0])
        uniqMAC+=1
        
        
        #print(t[0],locMAC)
        if locMAC==1:
            countLocMac +=1
        else:
            f.write(t[0]+'\n')
    ssidName=[]
    ssidAnzahl=[]
    
    for t in sorted(datssid.items(), key=lambda kv: kv[1]):
        
        if  t[0]!="" and not "Hacklabor" in t[0]:
            if t[0]!="":
                ssidName.append(t[0])
                ssidAnzahl.append(t[1])
            else:
                ssidName.append('Broadcast')
                ssidAnzahl.append(t[1])
        
        
        

    f.close()
    dName=['Gesamt','Einmalige MAC','Random MAC','Hersteller MAC','DS mit SSID','DS ohne SSID']
    df=pd.DataFrame()
    df['Legende']=dName

    dat=[]
    dat.append(NdwCount)
    dat.append(uniqMAC)
    dat.append(countLocMac)
    dat.append(uniqMAC-countLocMac)
    dat.append(countSSID)
    dat.append(NdwCount-countSSID)

    df['Data']=dat
    writeSheet(4,df)
    
    df=pd.DataFrame()
    
    df['SSID']=ssidName
    df['Anzahl']=ssidAnzahl
    writeSheet(3,df)
    sql = "SELECT * FROM eTwin"
    cursor.execute(sql)
    data12={}
    for dsatz in cursor:
        print(dsatz)
        data12,cnt =makeDict(cursor,3)
        print(data12)
    ssidName=[]
    ssidAnzahl=[]
    
    for t in sorted(data12.items(), key=lambda kv: kv[1]):
        ssidName.append(t[0])
        ssidAnzahl.append(t[1])
    df=pd.DataFrame()
    
    df['SSID']=ssidName
    df['Anzahl']=ssidAnzahl
    writeSheet(1,df)
    connection.close()
   
    time.sleep(120)

exit(0)
df=pd.DataFrame()

sql = "SELECT * FROM NDW "
cursor.execute(sql)
now=time.time()
data,cnt =makeDict(cursor,2)
print(data,cnt)
dat=[]
dat1=[]
mac=[]
mac1=[]
c=0
kleiner50=0
Größer50=0
Geskleiner50=0
GesGrößer50=0
index=[]
f = open("balcklist.txt",'w')
for t in sorted(data.items(), key=lambda kv: kv[1]):
    if t[1]>50:
        locMAC=getLocalMac(t[0])
        Größer50 +=1
        print('Gr',Größer50)
        if locMAC==0:
            m=getMacString(t[0])
            GesGrößer50+=int(t[1])
            f.write(t[0]+'\n')


            m1=OuiLookup().query(m)
            adr =m1[0][t[0].upper()]
            #print(adr)
            if adr != None:
                mac.append(adr)
   

        
        mac1.append(adr)
        dat.append(t[0])
        try:

            index.append(int([0][0:2],16)&2)
        except:
            index.append('na')
        dat1.append(t[1])
        c+=1 
        if c==300000:
            break
    else:
        kleiner50 +=1
        Geskleiner50+=int(t[1])
f.close()
data,cnt =makeDict(mac,-1)
cnt1=[]
vendor=[]
for t in sorted(data.items(), key=lambda kv: kv[1]):
    vendor.append(t[0])
    cnt1.append(t[1])


df["MAC"]=dat
df["Count"]=dat1
df['Vendor']=mac1
df['index']=index

writeSheet(0,df)
df=pd.DataFrame()
df["Vendor"]=vendor
df["cnt"]=cnt1
writeSheet(1,df)
df=pd.DataFrame()
da =[]
da.append(kleiner50)

df['weniger als 50 DS']=da
da =[]
da.append(Größer50)
df['mehr als 50 DS']=da
da =[]
da.append(Geskleiner50)

df['Absolute DS < 50 counts']=da
da =[]
da.append(GesGrößer50)
df['Absolute DS > 50 counts']=da
writeSheet(3,df)






print(time.time())
connection.close()