#include <Arduino.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

#include <Crypto.h>

bool LED_ON = 1;
unsigned long last_LED_TIME=0;

//#define Zuhause

#ifdef Zuhause
#include <secret.h>
WiFiClient espClient;
#else
#include <secret.h>
WiFiClientSecure espClient;
#endif
//String ssid = "WIFI@DB";
String ssid = "WIFIonICE";
//String ssid = "city.WLAN";
//String ssid = "Vodafone Hotspot";
//String ssid = "Telekom";

char station_mac[18];




extern "C" {
  #include <user_interface.h>
}




PubSubClient client(espClient);


String inputString = "";

// MQTT Last will and Testament
unsigned char willQoS = 0;
String willTopic = "Sniffer";
const char *willOnMessage = "online";
const char *willOffMessage = "offline";
String willClientID = "Sniffer";
boolean willRetain = true;



/* configuration  wifi */
//const char *ssid = "Hacklabor";


ESP8266WebServer server(80);

void handleRoot() { 
    server.send(200, "text/html", "<h1>You are connected</h1>");
    String addy = server.client().remoteIP().toString();
    Serial.println(addy);
}
void showConnectedDevices()
{
    auto client_count = wifi_softap_get_station_num();
    Serial.printf("Total devices connected = %d\n", client_count);

    auto i = 1;
    struct station_info *station_list = wifi_softap_get_station_info();
    while (station_list != NULL) {
        auto station_ip = IPAddress((&station_list->ip)->addr).toString().c_str();
        station_mac[18] = {0};
        sprintf(station_mac, "%02X:%02X:%02X:%02X:%02X:%02X", MAC2STR(station_list->bssid));
        Serial.printf("%d. %s %s", i++, station_ip, station_mac);
        String Topic= willTopic + "/";
        String Data =willClientID+","+ssid;
        Data +=",";
        Data += station_mac;
        Topic =   willTopic + "/eTwin/" + willClientID+ "/";
        client.publish(Topic.c_str(), Data.c_str());
        station_list = STAILQ_NEXT(station_list, next);
    }
    wifi_softap_free_station_info();
}


void Toggle_LED ()
{
  if (LED_ON){
    LED_ON=0;
    digitalWrite(D4,LOW);
  }
  else{
    LED_ON=1;
    digitalWrite(D4,HIGH);
  }
   
}


int MqttWrite(String data) {
int i;
String val[7];
String Topic= willTopic + "/";
//Topic =   willTopic + "/DEBUG/" + willClientID+ "/";
//client.publish(Topic.c_str(), data.c_str());
for (i=0;i<7;i++){
 int ind=data.indexOf(",");
if (ind==-1){
  val[i]=data;
  break;
}
val[i]=data.substring(0,ind);
data=data.substring(ind+1,data.length());
}



String Data =""; 
val[0]=willClientID;
for (int i=0;i<7;i++){

	Data += val[i]+ ",";
	
}




Topic =   willTopic + "/" + willClientID+ "/";
client.publish(Topic.c_str(), Data.c_str());
	

  return 0;

}

void callback(char *topic, unsigned char *payload, unsigned int length)
{
  enum key
  {
    SPEED = 0,
    BRIGHTNESS = 1,
    MODE = 2,
    COLOR = 3,
  };

  String command;
  int key;

  key = payload[0] - 48;
  for (unsigned int i = 1; i < length; i++)
  {
    command = command + String((char)payload[i]);
  }
  Serial.print( String(topic));
  Serial.print( String(key));
  Serial.print( command);

 
}

void reconnect()
{
  // Loop until we're reconnected
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection...");
    String topic= willTopic+"/";
    #ifdef Zuhause
if (client.connect(willClientID.c_str(),topic.c_str(), willQoS, willRetain, willOffMessage))
#else
if (client.connect(willClientID.c_str(),mqttUser, mqttPassword,topic.c_str(), willQoS, willRetain, willOffMessage))
#endif
    //if (client.connect(willClientID.c_str(),mqttUser, mqttPassword,topic.c_str(), willQoS, willRetain, willOffMessage))
    
    {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish(topic.c_str(), willOnMessage, willRetain); // ... and resubscribe
      client.subscribe(mainTopic);
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 500 seconds before retrying
      delay(2000);
    }
  }
}
String macToID(const uint8_t* mac)
{
  String result;
  for (int i = 3; i < 6; ++i) {
    result += String(mac[i], 16);
  }
  result.toUpperCase();
  return result;
}


void setup() {
  // set the WiFi chip to "promiscuous" mode aka monitor mode

pinMode(D4,OUTPUT);

  inputString.reserve(200);
  Serial.begin(921600);
    Serial.println("");
  Serial.println("TSRTA");
   
  WiFi.mode(WIFI_AP_STA);
  WiFi.begin(STASSID, STAPSK);
 
  Serial.println("");
  Serial.println("TSRTA");
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
 
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("ESP Board MAC Address:  ");
  Serial.println(WiFi.macAddress());
  String tmp =WiFi.macAddress();
  String id, hostname;
  uint8_t mac[6];

  WiFi.macAddress(mac);
  id=macToID(mac);
  willClientID+="_"+id;
     #ifdef Zuhause
client.setServer(MqttServer, 1883);
#else
  espClient.setInsecure();
client.setServer(MqttServer, 8883);
#endif
  
  client.setCallback(callback);
  
  WiFi.softAP(ssid.c_str(),NULL,1,0);
    IPAddress myIP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(myIP);
    server.on("/", handleRoot);

}

void loop() {
  digitalWrite(D4,HIGH);
   if (!client.connected())
  {
    reconnect();
  }
  client.loop();

 if(millis()-last_LED_TIME>1000){
last_LED_TIME=millis();
showConnectedDevices();
}
 
 while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar!= '\n' && inChar!= '\r' )
    {
    inputString += inChar;
    }
   
    if (inChar == '\n') {
     MqttWrite(inputString);
     inputString="";
     
     delay(10);
    }
     
  }

}