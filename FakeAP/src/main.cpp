#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

extern "C" {
  #include<user_interface.h>
}

/* configuration  wifi */
const char *ssid = "Hacklabor";
const char *ssid2 = "WIFI@DB";

ESP8266WebServer server(80);

void handleRoot() { 
    server.send(200, "text/html", "<h1>You are connected</h1>");
    String addy = server.client().remoteIP().toString();
    Serial.println(addy);
}
void showConnectedDevices()
{
    auto client_count = wifi_softap_get_station_num();
    Serial.printf("Total devices connected = %d\n", client_count);

    auto i = 1;
    struct station_info *station_list = wifi_softap_get_station_info();
    while (station_list != NULL) {
        auto station_ip = IPAddress((&station_list->ip)->addr).toString().c_str();
        char station_mac[18] = {0};
        sprintf(station_mac, "%02X:%02X:%02X:%02X:%02X:%02X", MAC2STR(station_list->bssid));
        Serial.printf("%d. %s %s", i++, station_ip, station_mac);
        station_list = STAILQ_NEXT(station_list, next);
    }
    wifi_softap_free_station_info();
}

void setup() {
    delay(1000);
    Serial.begin(115200);
    Serial.println();
    Serial.print("Configuring access point...");
    
    WiFi.softAP(ssid2,NULL,1,0);
    IPAddress myIP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(myIP);
    server.on("/", handleRoot);
    //server.begin();
    Serial.println("HTTP server started");  
}
 
void loop() {
    Serial.printf("Free Heap: %d Bytes\n", ESP.getFreeHeap());
    //server.handleClient();    
    showConnectedDevices();
    delay(1000);
}

